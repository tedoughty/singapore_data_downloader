package com.sti.singaporeneadownloader;

import com.google.gson.*;
import com.sti.justice.config.AppConfig;
import com.sti.justice.config.CommandLineConfig;
import com.sti.justice.config.PropertiesFileConfig;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

public class App {
    public static void main(String[] args) {
        Path currentPath = Paths.get(System.getProperty("user.dir"));
        Path filePath = Paths.get(currentPath.toString(), "src\\config.properties");
        File configFile = new File(filePath.toString());
        DateTime dts = new DateTime(DateTimeZone.forID("Asia/Singapore"));
        AppConfig<String,String> fileConfig = PropertiesFileConfig.createInstanceFromFiles(Collections.singletonList(configFile));
        CommandLineConfig commandConfig = CommandLineConfig.newBuilder().loadFromArgs(args).build();
        String downloadURL = fileConfig.get("downloadURL");
        String dateTimeAppendURL = fileConfig.get("dateTimeAppendURL");
        String saveLocation = fileConfig.get("saveLocation");
        String currentDateTime = commandConfig.get("dateTime", dts.toString("yyy-MM-dd'T'HH:mm:ss"));
        String currentDate = commandConfig.get("date", dts.toString("yyy-MM-dd"));

        try {
            URL neaURL = new URL(downloadURL + currentDateTime + dateTimeAppendURL + StringUtils.substring(currentDateTime, 0, 10));
            URLConnection request = neaURL.openConnection();
            request.connect();
            JsonParser neaJson = new JsonParser();
            JsonElement neaElement = neaJson.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject neaData = neaElement.getAsJsonObject().get("items").getAsJsonArray().get(0).getAsJsonObject();
            String neaTimestamp = neaData.get("timestamp").getAsString().split("\\+")[0];
            String neaFileName = String.valueOf(Paths.get(saveLocation, (neaTimestamp.replace(":", "-") + ".nea")));
            try (FileWriter file = new FileWriter(neaFileName)) {
                file.write(neaElement.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
